# Test pyodide

## Un simple terminal

{{ terminal() }}

## Un IDE horizontal vide

{{ IDE() }}

## Un IDE vertical vide

{{ IDEv() }}

## Un IDE horizontal avec fichier

{{ IDE('ex1') }}